export interface HotDog {
  id: number;
  name: string;
  description: string;
  expirationDate: string;
  backgroundURL: string;
}
