import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

import { ContactService } from './contact.service';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.less']
})
export class ContactComponent implements OnInit {

  contactForm: FormGroup;
  isDataSending: boolean;
  wasDataSent: boolean;

  constructor(private contactService: ContactService) { }

  ngOnInit() {
    this.contactForm = new FormGroup({
      'name': new FormControl(
        null,
        [
          Validators.required,
          Validators.pattern('[A-Za-z. -]{1,35}')
        ]),
      'email': new FormControl(
        null,
        [
          Validators.required,
          Validators.email
        ]),
      'comment': new FormControl(
        null,
        [ Validators.required ])
    });
  }

  onSubmit() {
    this.isDataSending = true;
    this.contactService
      .sendContactData(this.contactForm.value)
      .subscribe(data => {
        this.isDataSending = false;
        this.wasDataSent = true;
        setTimeout(() => this.wasDataSent = false, 2000);
      });
  }

  checkInvalidity(controlName: string): boolean {
    return this.contactForm.get(controlName).invalid && this.contactForm.get(controlName).dirty;
  }
}
