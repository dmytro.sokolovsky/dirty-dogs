import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ContactService {

  private url = 'https://formula-test-api.herokuapp.com/contact';

  constructor(private http: HttpClient) { }

  sendContactData(data) {
    return this.http.post(this.url, data);
  }
}
