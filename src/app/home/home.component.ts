import { Component, OnInit } from '@angular/core';

import { HomeService } from './home.service';
import { HotDog } from '../shared/hot-dog.model';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.less']
})
export class HomeComponent implements OnInit {

  hotDogs: HotDog[];

  constructor(private homeService: HomeService) { }

  ngOnInit() {
    this.homeService.getContent()
      .subscribe( (data: HotDog[]) => {
        this.hotDogs = this.filterByExpiration(data);
      });
  }

  filterByExpiration(items) {
    const currentDate = Date.parse(new Date().toDateString());
    return items.filter( item => {
      return Date.parse(item.expirationDate) > currentDate;
    });
  }

}
