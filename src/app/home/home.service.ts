import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class HomeService {

  private requestUrl = 'https://formula-test-api.herokuapp.com/menu';

  constructor(private http: HttpClient) { }

  getContent() {
    return this.http.get(this.requestUrl);
  }
}
