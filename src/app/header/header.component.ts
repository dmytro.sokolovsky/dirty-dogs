import { Component, OnInit } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';

import { filter } from 'rxjs/operators';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.less']
})
export class HeaderComponent implements OnInit {

  isMenuCollapsed: boolean;
  showHero: boolean;

  constructor(private router: Router) {
    this.router.events
      .pipe(filter((event) => event instanceof NavigationEnd))
      .subscribe((event) => {
        this.showHero = (event['url'] === '/');
      });
  }

  ngOnInit() {
    this.isMenuCollapsed = false;
  }

  showMenu() {
    this.isMenuCollapsed = !this.isMenuCollapsed;
  }
}
